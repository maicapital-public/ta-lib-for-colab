


%%bash

git clone --quiet https://gitlab.com/maicapital-public/ta-lib-for-colab.git
if [ ! -f /usr/include/ta-lib/ta_common.h ]; then
    cd ta-lib-for-colab
    tar -xzvf ta-lib-0.4.0-src.tar.gz
    cd ta-lib
    ./configure --prefix=/usr
    make
    make install
fi

import pip

def install(package):
    if hasattr(pip, 'main'):
        pip.main(['install', package])
    else:
        pip._internal.main(['install', package])

# Example
if __name__ == '__main__':
    install('pydantic')
    install('pendulum')
    install("ciso8601")

import matplotlib.pyplot as plt
import matplotlib as mpl
import pendulum
import pandas as pd
import datetime
import matplotlib.pyplot as plt
from pydantic import BaseModel
import talib



start = datetime.datetime(2015, 1, 1)
end = datetime.datetime(2019, 8, 15)


# In[ ]:


#@title
from typing import List, Dict, Set, Tuple
import math

class Commission(object):
    pass

class TradePercentage(Commission):
    def __init__(self, percentage):
        assert (percentage < 1)
        self.__percentage = percentage

    def calculate(self, price, amount):
        return price * abs(amount) * self.__percentage

class Inventory(BaseModel):
    inventory: List = []
    islong: bool = True

    def go_short(self):
        assert len(self.inventory) == 0
        self.islong = False

    def go_long(self):
        assert len(self.inventory) == 0
        self.islong = True

    def get_amount(self):
        if len(self.inventory) > 0:
            sign = 1 if self.islong else -1
            return sign * self.inventory[0][0]
        else:
            return 0

    def get_price(self):
        if len(self.inventory) > 0:
            return self.inventory[0][1]
        else:
            return 0

    def _entry(self, amount:float, price:float):
        ''' Entry amount must be positive'''
        assert amount > 0
        assert price > 0
        if len(self.inventory) == 0:
            self.inventory.append((amount, price))
        else:
            new_amount = self.inventory[0][0] + amount
            new_avg_price = (self.inventory[0][0] * self.inventory[0][1] + amount * price) / new_amount
            self.inventory[0] = (new_amount, new_avg_price)
        return amount * price

    def _exit(self, amount:float, price:float):
        ''' Exit amount must be positive'''
        assert amount > 0
        assert price > 0
        if len(self.inventory) > 0:
            inventory_amount = abs(self.get_amount())
            avg_price = self.get_price()
            if self.islong:
                realized_pnl = (price - avg_price) / avg_price
            else:
                realized_pnl = (avg_price - price) / avg_price
            realized_profit_pt = amount * realized_pnl
            if math.isclose(inventory_amount, amount):
                self.inventory = []
            else:
                self.inventory[0] = (inventory_amount - amount, avg_price)
            return (realized_profit_pt, realized_pnl, avg_price, amount * price)

    def long(self, amount:float, price:float):
        assert self.islong
        return self._entry(abs(amount), price)

    def close(self, amount:float, price:float):
        assert self.islong
        return self._exit(abs(amount), price)

    def short(self, amount:float, price:float):
        assert not self.islong
        return self._entry(abs(amount), price)

    def cover(self, amount:float, price:float):
        assert not self.islong
        return self._exit(abs(amount), price)

class Position(BaseModel):
    '''
    Keep records of a position of trading pairs
    Attributes:
        strategy_exposure (float): strategy_exposure is the nominal ratio for exposures in markets from -1 to 1
        base_rate (float): base_rate is the ratio that transfer the fund to the base trading pairs (can be changed over time)
        fund (float): Fund in USD
        amount (float): Assets amounts
        target_exposure (float): Target exposure
        fee (float): Total fee incurred
        trade_log (List): trade log
        trade_profit (List): trade profit log
        balance_log (List): balance log
    '''
    strategy_exposure: float = 0
    base_rate: float = 1.0
    fund: float = 0
    fee: float = 0
    leverage: float = 1
    inv: Inventory = Inventory()
    commision: Commission = TradePercentage(0.001)
    trade_log: List = []
    trade_profit: List = []
    balance_log: List = []
    timestamp_log: List = []

    class Config:
        arbitrary_types_allowed=True

    def enough_amount(self, amount:float):
        return abs(amount) <= abs(self.get_amount()) + PRECISION

    def enough_cash(self, cash:float):
        return cash <= self.fund + PRECISION

    @staticmethod
    def cal_nav(cash, amount, price):
        return cash + amount * price # Should be +ve

    @staticmethod
    def cal_exposure(cash, amount, price):
        """ Since the exposure can be exceeds (-1,1) based on the price of the assets, 
        the real exposure shall be calculated at the time of trade
        """
        if amount == 0:
            return 0
        nav = Position.cal_nav(cash, amount, price)
        if nav <= 0:
            return 0
        else:
            return 1 - cash / nav

    def get_amount(self):
        return self.inv.get_amount()

    def get_gav(self, price):
        return Position.cal_nav(self.fund, self.get_amount(), price)

    def get_nav(self, price):
        return self.get_gav(price) - self.fee

    def long(self, amount:float, price:float, timestamp:datetime.datetime=None, notes:str=""):
        if self.inv.get_amount() == 0:
            self.inv.go_long()
        amount = abs(amount)
        cash_changed = self.inv.long(abs(amount), price)
        fee_to_pay = self.commision.calculate(price, amount)
        # Updates records
        self.fund += -1 * abs(cash_changed)
        self.fee += fee_to_pay
        self.trade_log.append({
            'timestamp':timestamp,
            'amount':amount,
            'fee':fee_to_pay,
            'price':price,
            'trade':'LONG',
            'notes':notes})
        return

    def short(self, amount:float, price:float, timestamp:datetime.datetime=None, notes:str=""):
        if self.inv.get_amount() == 0:
            self.inv.go_short()
        amount = abs(amount)
        cash_changed = self.inv.short(abs(amount), price)
        cash_changed = abs(cash_changed)
        fee_to_pay = self.commision.calculate(price, amount)
        # Updates records
        self.fund += cash_changed
        self.fee += fee_to_pay
        self.trade_log.append({
            'timestamp':timestamp,
            'amount':-amount,
            'fee':fee_to_pay,
            'price':price,
            'trade':'SHORT',
            'notes':notes})

    def close(self, amount:float, price:float, timestamp:datetime.datetime=None, notes:str=""):
        amount = abs(amount)
        realized_profit_pt, realized_pnl, avg_price, cash_changed = self.inv.close(abs(amount), price)
        realized_gross_profit = realized_profit_pt * self.base_rate * avg_price
        fee_to_pay = self.commision.calculate(price, amount)
        total_fee = fee_to_pay + self.commision.calculate(avg_price, amount)
        cash_changed = abs(cash_changed)
        # Updates records
        self.fund += cash_changed
        self.fee += fee_to_pay
        self.trade_log.append({
            'timestamp':timestamp,
            'amount':-abs(amount),
            'fee':fee_to_pay,
            'price':price,
            'trade':'CLOSE',
            'notes':notes})
        self.trade_profit.append({
            'timestamp':timestamp,
            'amount':abs(amount),
            'exit_price':price,
            'enter_price':avg_price,
            'realized_gross_profit':realized_gross_profit,
            'realized_profit_pt':realized_profit_pt,
            'realized_pnl':realized_pnl,
            'total_fee': total_fee,
            'trade':'LONG'})

    def cover(self, amount:float, price:float, timestamp:datetime.datetime=None, notes:str=""):
        amount = abs(amount)
        realized_profit_pt, realized_pnl, avg_price, cash_changed = self.inv.cover(abs(amount), price)
        realized_gross_profit = realized_profit_pt * self.base_rate * avg_price
        fee_to_pay = self.commision.calculate(price, amount)
        total_fee = fee_to_pay + self.commision.calculate(avg_price, amount)
        cash_changed = -1 * abs(cash_changed)
        # Updates records
        self.fund += cash_changed
        self.fee += fee_to_pay
        self.trade_log.append({
            'timestamp':timestamp,
            'amount':abs(amount),
            'fee':fee_to_pay,
            'price':price,
            'trade':'COVER',
            'notes':notes})
        self.trade_profit.append({
            'timestamp':timestamp,
            'amount':-abs(amount),
            'exit_price':price,
            'enter_price':avg_price,
            'realized_gross_profit':realized_gross_profit,
            'realized_profit_pt':realized_profit_pt,
            'realized_pnl':realized_pnl,
            'total_fee': total_fee,
            'trade':'SHORT'})

    def update_base_rate(self, rate):
        self.base_rate = rate

    def set_commision(self, commision: Commission):
        self.commision = commision

    def extract_fund(self):
        fund = self.fund
        self.fund = 0
        return fund

    def deposit_fund(self, fund):
        self.fund = fund

    def summary(self):
        return {'fund':self.fund, 'amount':self.get_amount(), 'strategy_exposure':self.strategy_exposure, 'fee':self.fee, 'base_rate':self.base_rate}

    def allocate(self, new_exposure: float, price: float, timestamp=None, notes=""):
        ''' To allocate the fund into specific exposure amount for the following case:
| old_exposure | new_exposure | conditions                                                            | amount_changed                            |
|--------------|--------------|-----------------------------------------------------------------------|-------------------------------------------|
| +ve          | 0            | old_exposure > 0 and new_exposure == 0                                | amount                                    |
| +ve          | more +ve     | old_exposure > 0 and new_exposure > 0 and new_exposure > old_exposure | nav * (new_exposure - exposure) / price   |
| +ve          | less +ve     | old_exposure > 0 and new_exposure > 0 and new_exposure < old_exposure | nav * (new_exposure - exposure) / price   |
| +ve          | -ve          | old_exposure > 0 and new_exposure < 0                                 | amount then nav * (new_exposure) / price  |
| 0            | +ve          | old_exposure == 0 and new_exposure > 0                                | nav * (new_exposure) / price              |
| 0            | -ve          | old_exposure == 0 and new_exposure < 0                                | nav * (new_exposure) / price              |
| -ve          | 0            | old_exposure < 0 and new_exposure == 0                                | amount                                    |
| -ve          | more -ve     | old_exposure < 0 and new_exposure < 0 and new_exposure < old_exposure | nav * (exposure - new_exposure) / price   |
| -ve          | less -ve     | old_exposure < 0 and new_exposure < 0 and new_exposure > old_exposure | nav * (exposure -  new_exposure ) / price |
| -ve          | +ve          | old_exposure < 0 and new_exposure > 0                                 | amount then nav * (new_exposure) / price  |
        '''
        old_exposure = self.strategy_exposure
        amount = self.inv.get_amount()
        exposure = Position.cal_exposure(self.fund, amount, price)
        nav = Position.cal_nav(self.fund, amount, price)

        # Return if old_exposure is very close to new_exposure
        if math.isclose(old_exposure, new_exposure):
            return None

        # Different case
        if old_exposure > 0:
            if  math.isclose(new_exposure, 0):
                amount_changed = amount
                self.close(amount_changed, price, timestamp=timestamp, notes=notes)
            elif new_exposure > 0 and new_exposure > old_exposure:
                amount_changed = nav * (new_exposure - exposure) / price
                self.long(amount_changed, price, timestamp=timestamp, notes=notes)
            elif new_exposure > 0 and new_exposure < old_exposure:
                amount_changed = nav * (new_exposure - exposure) / price
                self.close(amount_changed, price, timestamp=timestamp, notes=notes)
            elif new_exposure < 0:
                # Close 
                amount_changed = amount
                self.close(amount_changed, price, timestamp=timestamp, notes=notes)
                # Short
                amount_changed = nav * new_exposure / price
                self.short(amount_changed, price, timestamp=timestamp, notes=notes)
        elif math.isclose(old_exposure, 0):
            if new_exposure > 0:
                amount_changed = nav * (new_exposure) / price
                self.long(amount_changed, price, timestamp=timestamp, notes=notes)
            elif new_exposure < 0:
                amount_changed = nav * (new_exposure) / price
                self.short(amount_changed, price, timestamp=timestamp, notes=notes)
        elif old_exposure < 0:
            if math.isclose(new_exposure, 0):
                amount_changed = amount
                self.cover(amount_changed, price, timestamp=timestamp, notes=notes)
            elif new_exposure < 0 and new_exposure > old_exposure:
                amount_changed = nav * (exposure - new_exposure) / price 
                self.cover(amount_changed, price, timestamp=timestamp, notes=notes)
            elif new_exposure < 0 and new_exposure < old_exposure:
                amount_changed = nav * (exposure - new_exposure) / price 
                self.short(amount_changed, price, timestamp=timestamp, notes=notes)
            elif new_exposure > 0:
                # Cover
                amount_changed = amount
                self.cover(amount_changed, price, timestamp=timestamp, notes=notes)
                # Long
                amount_changed = nav * (new_exposure) / price
                self.long(amount_changed, price, timestamp=timestamp, notes=notes)
        # Update the strategy exposure
        self.strategy_exposure = new_exposure
        return

    def end_date(self, timestamp, price):
        summary = self.summary()
        summary.update({'timestamp':timestamp})
        summary.update({'price':price})
        summary.update({'gav': self.get_gav(price)})
        summary.update({'nav': self.get_nav(price)})
        summary.update({'exposure': Position.cal_exposure(summary['fund'], summary['amount'], price)})
        self.balance_log.append(summary)
        self.timestamp_log.append(timestamp)

    def get_trade_profit(self):
        return pd.DataFrame(self.trade_profit)

    def get_trade_log(self):
        return pd.DataFrame(self.trade_log)

    def get_balance_log(self):
        return pd.DataFrame(self.balance_log, index=self.timestamp_log)


# In[ ]:


from collections import OrderedDict
import pandas as pd
import numpy as np
from copy import copy

########################################################################
#### Statistics
#### Initilize with strategy config and dict of ``positions`` of trading pairs (from trade.py)
####
#### Usage:
#### stat = Statistics(config, positions)
#### stat.calculate()
#### stat.trade_summary()
#### stat.nav_summary()
#### stat.report()
########################################################################


class Statistics(object):
    def __init__(self, config, positions, fixed_cash=0):
        self.config = config
        self.positions = positions
        self.balance_log = None
        self.equity = None
        self.trade_profit = None
        self.fixed_cash = fixed_cash

    def calculate(self):
        symbols = list(self.positions.keys())
        self.equity = pd.concat([self.positions[s].get_balance_log()['nav'] for s in symbols], keys=symbols, axis=1).sum(axis=1) + self.fixed_cash
        self.gav = pd.concat([self.positions[s].get_balance_log()['gav'] for s in symbols], keys=symbols, axis=1).sum(axis=1) + self.fixed_cash
        self.balance_log = pd.concat([self.positions[s].get_balance_log() for s in symbols], keys=symbols, axis=1)
        self.trade_profit = pd.concat([self.positions[s].get_trade_profit() for s in symbols], keys=symbols)
        self.trade_profit.reset_index(inplace=True)
        self.trade_profit = self.trade_profit.set_index('timestamp').sort_index()

    def get_long_trades(self):
        return self.trade_profit[self.trade_profit['trade'] == 'LONG']

    def get_short_trades(self):
        return self.trade_profit[self.trade_profit['trade'] == 'SHORT']

    def nav_summary(self, raw=False):
        start_time = self.equity.index[0]
        end_time = self.equity.index[-1]
        results = OrderedDict([
            ["Initial Capital", self.config.fund],
            ["Ending Capital", self.equity[-1]],
            ["Trade Start", start_time],
            ["Trade End", end_time],
            ["Trade Days", end_time - start_time],
            ["Gross Profit", self.gav[-1] - self.config.fund],
            ["Gross Profit %", (self.gav[-1] - self.config.fund) / self.config.fund * 100],
            ["Net Profit", self.equity[-1] - self.config.fund],
            ["Net Profit %", (self.equity[-1] - self.config.fund) / self.config.fund * 100],
            ["Maximum Drawdown %", max_drawdown(self.equity) * 100],
            ["Annual Return %", annualized_return(self.equity, self.config.fund) * 100],
            ["Sharpe Ratio", sharpe_ratio(self.equity)],
            ["Trading Fee", self.balance_log.swaplevel(axis=1)['fee'].iloc[-1][0]]
        ])
        if raw:
            return results
        else:
            return pd.DataFrame([results], columns=results.keys()).T

    def monthly_return(self):
        df = copy(self.equity.to_frame(name='returns'))
        df['year'] = df.index.year
        df['month'] = df.index.month
        end_date_of_month = df.asfreq('BM').set_index(['year', 'month'])
        first_date_of_month = df.asfreq('BMS').set_index(['year', 'month'])
        pct = end_date_of_month.pct_change()
        if pd.isna(pct['returns'][0]):
            pct['returns'][0] = end_date_of_month['returns'][0] / first_date_of_month['returns'][0] - 1
        return pct

    def trade_summary(self, raw=False):
        columns = ['All', 'Long', 'Short']
        long_trades = self.get_long_trades()
        short_trades = self.get_short_trades()
        def cal_trades(trades):
            winning_mask = trades['realized_pnl']>0
            lossing_mask = trades['realized_pnl']<=0
            return OrderedDict([
                ["Total Trades", len(trades)],
                ["Avg. Profit/Loss", trades['realized_gross_profit'].mean()],
                ["Avg. Profit/Loss %", trades['realized_pnl'].mean()],
                ["Winning Trades", winning_mask.sum()],
                ["Winning Trades %", winning_mask.sum()/len(trades)*100],
                ["Winning Trades Avg. Profit", trades[winning_mask]['realized_gross_profit'].mean()],
                ["Winning Trades Avg. Profit %", trades[winning_mask]['realized_pnl'].mean()],
                ["Lossing Trades", lossing_mask.sum()],
                ["Lossing Trades %", lossing_mask.sum()/len(trades)*100],
                ["Lossing Trades Avg. Profit", trades[lossing_mask]['realized_gross_profit'].mean()],
                ["Lossing Trades Avg. Profit %", trades[lossing_mask]['realized_pnl'].mean()],
            ])
        results = [
            cal_trades(self.trade_profit),
            cal_trades(long_trades),
            cal_trades(short_trades),
        ]
        if raw:
            return results
        else:
            return pd.DataFrame(results, index=columns, columns=results[0].keys()).T

    def report(self):
        pass

########################################################################
#### Metrics
#### Functions used to compute metrics of portfolios
####
########################################################################

def max_drawdown(ser):
    """A drawdown is the peak-to-trough decline during a specific recorded period
    https://stackoverflow.com/questions/22607324/start-end-and-duration-of-maximum-drawdown-in-python
    """
    val = ser.values
    peak = 0
    drawdown = 0
    
    for i in range(len(val)):
      val0 = i+1
      while val0 < len(val):
        diff = 1 - (val[val0] / val[i])
        if diff > drawdown:
          peak = i
          drawdown = diff
        else:
          pass
        val0 = val0 + 1
      return drawdown

def annualized_return(ts, initial_fund=None):
    '''An annualized total return is the geometric average amount of money earned by an investment 
    each year over a given time period. It is calculated as a geometric average to show what an 
    investor would earn over a period of time if the annual return was compounded. 
    An annualized total return provides only a snapshot of an investment's performance 
    and does not give investors any indication of its volatility.
    https://www.investopedia.com/terms/a/annualized-total-return.asp
    '''
    if initial_fund is None:
        initial_fund = ts[0]
    ar = (ts[-1] - initial_fund) / initial_fund
    ar = (1 + ar) ** (365*24 / len(ts.index)) - 1
    return ar

def sharpe_ratio(ts, rff=0.03):
    #Rolling profit
    r = ts.pct_change()
    #Annualized return
    ar = annualized_return(ts)
    #Sharpe ratio
    return (ar - rff)/(r.std()*np.sqrt(365*24))


# # Get Data

# In[ ]:


data = pd.read_csv("http://www.cryptodatadownload.com/cdd/Kraken_BTCUSD_1h.csv", skiprows=1)
data["Date"] = pd.to_datetime(data["Date"], format='%Y-%m-%d %I-%p')
data = data.set_index('Date')
data = data.sort_index()


# # DataManager

# In[ ]:


class StrategyParameters:
    fund = 10e5
    start_time = "2015-01-01"
    end_time = "2019-12-31"
    long_ratio = 1
    short_ratio = -0.2
    cut_loss = 0.15
    neutral_when_criteria_not_meet = False # No Long and Short position, i.e. exposure = 0
    results_file='weekend/result_{}.xlsx'
    symbols = ["BTCUSD"] #"ETHUSD", "EOSUSD", "BCHUSD", "LTCUSD", "XMRUSD", "XRPUSD"]
    allocations = {
        'BTCUSD': 1
        #"ETHUSD": 0.16, 
        #"EOSUSD": 0.08, 
        #"BCHUSD": 0.03, 
        #"LTCUSD": 0.01, 
        #"XMRUSD": 0.01, 
        #"XRPUSD": 0.01,
    }
  
    def __init__(self, hour):
        self.hour = hour


# In[ ]:


def select_tradeperiod(ts, start_time, end_time, pad=True):
    if pad:
        start_time = start_time - datetime.timedelta(365)
    if start_time < ts.index[0]: start_time = ts.index[0]
    if end_time > ts.index[-1]: end_time = ts.index[-1]
    ts = ts.loc[start_time:end_time]
    return ts

class Signal:
    long, short, close, cutloss = 'long', 'short', 'close', 'cutloss'

class DataManager(object):
    def __init__(self, config):
        self.config = config
        self.data = {}
        self.signal = {}
        self.position = {}

    def add_data(self, symbol, data):
        self.data[symbol] = data
        self.signal[symbol] = self.gen_signal(data)
        
    def gen_signal(self, ts):
        price = ts['Open']
        hl2 = (ts['High'] + ts['Low']) / 2
        close = ts['Close']                  
        ts["day_of_week"] = ts.index.dayofweek
        ts["hour"] = ts.index.hour
         
        day_signal = (ts["day_of_week"] == 4) & (ts["hour"] == self.config.hour)
        day_signal2 = (ts["day_of_week"] == 5) & (ts["hour"] == self.config.hour)                     
        day_signal3 = (ts["day_of_week"] == 6) & (ts["hour"] == self.config.hour)                      
        day_signal4 = (ts["day_of_week"] == 0) & (ts["hour"] == self.config.hour)                             

        long_signal = day_signal
        short_signal = day_signal3
        close_signal = day_signal2
        cover_signal = day_signal4
        
        return pd.concat([
            price.rename('price'), 
            close.rename('close'),
            long_signal.rename('long_signal'),
            close_signal.rename('close_signal'),
            short_signal.rename('short_signal'),
            cover_signal.rename('cover_signal'),
        ], axis =1)
    
    def backtest(self, symbol):
        pos = self.position[symbol] = Position(fund=self.config.fund)
        algo_data = self.signal.get(symbol)
        exposure = 0
        start_flag = True
        need_trade = True
        algo_datetime = algo_data.index
        high_water_mark = 0
        cut_loss = False
        stop_buy = 0

        for i in range(len(algo_data)):
            dt = algo_datetime[i]    # Datetime (Index)
            df = algo_data.iloc[i]
            price, close, long_signal, close_signal, short_signal, cover_signal = df.values
            nav = pos.get_nav(price)
            amount = pos.get_amount()
            if long_signal:
                pos.long(amount=nav/price, price=price)
                start_flag = False
            if not start_flag and close_signal:
                pos.close(amount=amount, price=price)
            # End Date
            pos.end_date(dt, close)
        return pos


# In[ ]:


df = pd.DataFrame()
x = list(range(0,24))
for time in x:
  dm = DataManager(StrategyParameters(time))
  dm.add_data("BTCUSD", data)
  result = dm.backtest("BTCUSD")
  s = Statistics(StrategyParameters(time), dm.position)
  s.calculate()
  df2 = s.nav_summary().rename(columns = {0:time})
  df = pd.concat([df, df2], axis=1)
  #print(drawdowns(result.get_balance_log()))
  #result.get_balance_log()['nav'].plot()
  #print(s.nav_summary())
  #print(result.get_balance_log().T)


# In[ ]:





# In[ ]:


print(df.T)

df.T.to_csv("Study.csv")